package com.TrainingManagement.Training.user.dto.notificationDto;

import com.TrainingManagement.Training.course.model.Module;

import java.util.List;

public class NotificationCreateDto {
    private List<Module> listOfModules;
    private String subject;
    private String content;

    public List<Module> getListOfModules() {
        return listOfModules;
    }

    public void setListOfModules(List<Module> listOfModules) {
        this.listOfModules = listOfModules;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
