package com.TrainingManagement.Training.user.dto.userDto;

import com.TrainingManagement.Training.course.model.Course;
import com.TrainingManagement.Training.course.model.CourseInstance;
import com.TrainingManagement.Training.user.model.Notification;
import com.TrainingManagement.Training.user.model.UserType;

import java.util.List;

public class UserReturnDto {
    private Integer id;
    private String userName;
    private UserType type;
    private String firstName;
    private String lastName;
    private Boolean active;
    private CourseInstance course;
    private List<Notification> listOfNotifications;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;

    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public CourseInstance getCourse() {
        return course;
    }

    public void setCourse(CourseInstance course) {
        this.course = course;
    }

    public List<Notification> getListOfNotifications() {
        return listOfNotifications;
    }

    public void setListOfNotifications(List<Notification> listOfNotifications) {
        this.listOfNotifications = listOfNotifications;
    }
}
