package com.TrainingManagement.Training.user.dto.applicationOfUserDto;

import com.TrainingManagement.Training.course.model.Course;
import com.TrainingManagement.Training.user.model.User;


public class ApplicationOfUserCreateDto {
    private Integer userId;
    private String dateOfApplication;
    private Integer courseId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDateOfApplication() {
        return dateOfApplication;
    }

    public void setDateOfApplication(String dateOfApplication) {
        this.dateOfApplication = dateOfApplication;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }
}
