package com.TrainingManagement.Training.user.dto.notificationDto;

import com.TrainingManagement.Training.course.model.Module;
import com.TrainingManagement.Training.user.model.User;

import java.util.List;

public class NotificationReturnDto {

    private List<Module> listOfModules;
    private String subject;
    private String content;
    private User user;

    public List<Module> getListOfModules() {
        return listOfModules;
    }

    public void setListOfModules(List<Module> listOfModules) {
        this.listOfModules = listOfModules;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
