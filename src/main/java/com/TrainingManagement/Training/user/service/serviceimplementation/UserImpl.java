package com.TrainingManagement.Training.user.service.serviceimplementation;


import com.TrainingManagement.Training.user.dto.userDto.UserCreateDto;
import com.TrainingManagement.Training.user.dto.userDto.UserReturnDto;
import com.TrainingManagement.Training.user.dto.userLoginDto.UserLoginDto;
import com.TrainingManagement.Training.user.mapper.UserMapper;
import com.TrainingManagement.Training.user.model.User;
import com.TrainingManagement.Training.user.repository.UserRepository;
import com.TrainingManagement.Training.user.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserImpl implements UserService {
    private final UserRepository userRepository;

    public UserImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserReturnDto createUser(UserCreateDto userCreateDto) {
        return UserMapper.toReturnDto(userRepository.save(UserMapper.toEntity(userCreateDto)));
    }

    @Override
    public UserReturnDto findById(Integer id) {
      return UserMapper.toReturnDto(userRepository.findById(id).orElseThrow(()-> new RuntimeException("User not found")));
    }

    @Override
    public UserReturnDto login(UserLoginDto userLoginDto) {
        User user = userRepository.findByUsername(userLoginDto.getUsername());
//        User user = findByUsernameV2(userLoginDto.getUsername());
        if(user != null && user.getPassword().equals(userLoginDto.getPassword())){
            return UserMapper.toReturnDto(user);
        }
        return null;
    }

    @Override
    public User userFindById(Integer id) {
        return userRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @Override
    public User findByUsernameV2(String username) {
        List<User> userList = userRepository.findAll();
        for(User user: userList){
            if(user.getUsername().equals(username)){
                return user;
            }
        }
        return null;
    }

    @Override
    public void update(User user) {
        userRepository.save(user);
    }
}
