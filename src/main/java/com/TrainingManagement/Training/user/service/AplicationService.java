package com.TrainingManagement.Training.user.service;

import com.TrainingManagement.Training.user.dto.applicationOfUserDto.ApplicationOfUserCreateDto;
import com.TrainingManagement.Training.user.dto.applicationOfUserDto.ApplicationOfUserReturnDto;
import org.springframework.stereotype.Service;

@Service
public interface AplicationService {
    ApplicationOfUserReturnDto createApplication(ApplicationOfUserCreateDto applicationOfUserCreateDto);

    ApplicationOfUserReturnDto returnAplication(String username);
}
