package com.TrainingManagement.Training.user.service;

import com.TrainingManagement.Training.user.dto.userDto.UserCreateDto;
import com.TrainingManagement.Training.user.dto.userDto.UserReturnDto;
import com.TrainingManagement.Training.user.dto.userLoginDto.UserLoginDto;
import com.TrainingManagement.Training.user.model.User;
import org.springframework.stereotype.Service;


public interface UserService {
    UserReturnDto createUser(UserCreateDto userCreateDto);
    UserReturnDto findById(Integer id);
    UserReturnDto login(UserLoginDto userLoginDto);
    User userFindById(Integer id);
    User findByUsernameV2(String username);
    void update(User user);


}
