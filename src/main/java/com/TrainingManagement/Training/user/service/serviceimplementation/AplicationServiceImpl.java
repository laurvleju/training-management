package com.TrainingManagement.Training.user.service.serviceimplementation;

import com.TrainingManagement.Training.course.model.CourseInstance;
import com.TrainingManagement.Training.course.repository.CourseInstanceRepository;
import com.TrainingManagement.Training.course.service.serviceImplementation.CourseInstanceImpl;
import com.TrainingManagement.Training.user.dto.applicationOfUserDto.ApplicationOfUserCreateDto;
import com.TrainingManagement.Training.user.dto.applicationOfUserDto.ApplicationOfUserReturnDto;
import com.TrainingManagement.Training.user.dto.userDto.UserReturnDto;
import com.TrainingManagement.Training.user.mapper.ApplicationMapper;
import com.TrainingManagement.Training.user.mapper.UserMapper;
import com.TrainingManagement.Training.user.model.ApplicationOfUser;
import com.TrainingManagement.Training.user.model.User;
import com.TrainingManagement.Training.user.repository.ApplicationOfUserRepository;
import com.TrainingManagement.Training.user.service.AplicationService;
import com.TrainingManagement.Training.user.service.UserService;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class AplicationServiceImpl implements AplicationService {

    private final ApplicationOfUserRepository applicationOfUserRepository;
    private final CourseInstanceRepository courseInstanceRepository;
    private final UserService userService;
    private CourseInstanceImpl courseInstanceImpl;

    public AplicationServiceImpl(ApplicationOfUserRepository applicationOfUserRepository, CourseInstanceRepository courseInstanceRepository, UserService userService) {
        this.applicationOfUserRepository = applicationOfUserRepository;
        this.courseInstanceRepository = courseInstanceRepository;
        this.userService = userService;
    }

    @Override
    public ApplicationOfUserReturnDto createApplication(ApplicationOfUserCreateDto applicationOfUserCreateDto) {
        boolean accepted = false;
        User user = userService.userFindById(applicationOfUserCreateDto.getUserId());
        List<CourseInstance> course = (List<CourseInstance>) courseInstanceRepository.findAll();
        CourseInstance courseInstanceUser = new CourseInstance();

        for (CourseInstance courseInstance : course) {
            if (courseInstance.getCourse().getId().equals(applicationOfUserCreateDto.getCourseId())) {
                courseInstanceUser = courseInstance;
                SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date userDate = sdformat.parse(applicationOfUserCreateDto.getDateOfApplication());
                    Date curseDate = sdformat.parse(courseInstance.getStartDate());
                    if ((curseDate.after(userDate)) && courseInstance.getUsers().size() < 15) {
                        accepted = true;
                        courseInstance.getUsers().add(user);
                    } else {
                        accepted = false;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        }

        if (accepted) {
            ApplicationOfUser a = ApplicationMapper.toEntity(user, courseInstanceUser, applicationOfUserCreateDto.getDateOfApplication(), true);
            applicationOfUserRepository.save(a);

            /*
                update pentru user sa nu primesc "deadlock error"
             */
            user.setActive(true);
            user.setCourse(courseInstanceUser);
            userService.update(user);

            return ApplicationMapper.toReturn(a);
        }
        return ApplicationMapper.toReturn(ApplicationMapper.toEntity(user, courseInstanceUser, applicationOfUserCreateDto.getDateOfApplication(), accepted));
    }

    @Override
    public ApplicationOfUserReturnDto returnAplication(String username) {
        return ApplicationMapper.toReturn(applicationOfUserRepository.findById(userService.findByUsernameV2(username).getId()).orElseThrow(RuntimeException::new));
    }
}
