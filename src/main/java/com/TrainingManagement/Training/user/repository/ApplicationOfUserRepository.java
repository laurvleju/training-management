package com.TrainingManagement.Training.user.repository;


import com.TrainingManagement.Training.user.model.ApplicationOfUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationOfUserRepository extends CrudRepository<ApplicationOfUser,Integer> {
}
