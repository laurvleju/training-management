package com.TrainingManagement.Training.user.repository;


import com.TrainingManagement.Training.user.model.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends CrudRepository<Notification,Integer> {
}
