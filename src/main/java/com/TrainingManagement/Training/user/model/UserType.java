package com.TrainingManagement.Training.user.model;

public enum UserType {
    ADMIN,
    LEADER,
    PARTICIPANT
}
