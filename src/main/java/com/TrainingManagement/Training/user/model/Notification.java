package com.TrainingManagement.Training.user.model;
import com.TrainingManagement.Training.course.model.Module;

import javax.persistence.*;
import java.util.List;

@Entity
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Integer id;

    @OneToMany(mappedBy = "notification")
    private List<Module> listOfModules;

    @Column(name = "Subject")
    private String subject;

    @Column(name = "Content")
    private String content;

    @ManyToOne
    private User user;

    public Integer getId() {
        return id;
    }

    public List<Module> getListOfModules() {
        return listOfModules;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setListOfModules(List<Module> listOfModules) {
        this.listOfModules = listOfModules;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
