package com.TrainingManagement.Training.user.model;

import com.TrainingManagement.Training.course.model.CourseInstance;

import javax.persistence.*;

@Entity
public class ApplicationOfUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "date_of_application")
    private String dateOfApplication;

    @OneToOne
    @JoinColumn(name = "course_id")
    private CourseInstance course;

    @Column
    private Boolean accepted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDateOfApplication() {
        return dateOfApplication;
    }

    public void setDateOfApplication(String dateOfApplication) {
        this.dateOfApplication = dateOfApplication;
    }

    public CourseInstance getCourse() {
        return course;
    }

    public void setCourse(CourseInstance course) {
        this.course = course;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }
}
