package com.TrainingManagement.Training.user.model;

import com.TrainingManagement.Training.course.model.Course;
import com.TrainingManagement.Training.course.model.CourseInstance;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Integer id;

    @Column(name = "Username")
    private String username;

    @Column(name = "Password")
    private String password;

    @Column(name = "Type")
    @Enumerated(EnumType.STRING)
    private UserType type;

    @Column(name = "First_name")
    private String firstName;

    @Column(name = "Last_name")
    private String lastName;

    @Column(name = "Active")
    private Boolean active;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private CourseInstance course;

    @OneToOne(mappedBy = "user")
    private ApplicationOfUser applicationOfUser;


    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public CourseInstance getCourse() {
        return course;
    }

    public void setCourse(CourseInstance course) {
        this.course = course;
    }
}
