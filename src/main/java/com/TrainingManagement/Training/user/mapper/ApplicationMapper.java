package com.TrainingManagement.Training.user.mapper;

import com.TrainingManagement.Training.course.model.Course;

import com.TrainingManagement.Training.course.model.CourseInstance;
import com.TrainingManagement.Training.user.dto.applicationOfUserDto.ApplicationOfUserReturnDto;
import com.TrainingManagement.Training.user.model.ApplicationOfUser;
import com.TrainingManagement.Training.user.model.User;

public class ApplicationMapper {

    public static ApplicationOfUser toEntity(User user, CourseInstance course, String date, Boolean accepted){
        ApplicationOfUser application = new ApplicationOfUser();
        application.setCourse(course);
        application.setUser(user);
        application.setDateOfApplication(date);
        application.setAccepted(accepted);
        return application;
    }

    public static ApplicationOfUserReturnDto toReturn(ApplicationOfUser application){
        ApplicationOfUserReturnDto applicationOfUserReturnDto = new ApplicationOfUserReturnDto();

        applicationOfUserReturnDto.setUserName(application.getUser().getFirstName());
        applicationOfUserReturnDto.setCourseName(application.getCourse().getName());
        applicationOfUserReturnDto.setDateOfApplication(application.getCourse().getStartDate());
        applicationOfUserReturnDto.setAccepted(application.getAccepted());
        return applicationOfUserReturnDto;
    }
}
