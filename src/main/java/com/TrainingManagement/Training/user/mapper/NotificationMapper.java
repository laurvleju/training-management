package com.TrainingManagement.Training.user.mapper;

import com.TrainingManagement.Training.user.dto.notificationDto.NotificationCreateDto;
import com.TrainingManagement.Training.user.dto.notificationDto.NotificationReturnDto;
import com.TrainingManagement.Training.user.model.Notification;

public class NotificationMapper {

    public static Notification toEntity(NotificationCreateDto notificationCreateDto){
        Notification notification=new Notification();
        notification.setContent(notificationCreateDto.getContent());
        notification.setSubject(notificationCreateDto.getSubject());
        notification.setListOfModules(notificationCreateDto.getListOfModules());
        return  notification;
    }

    public static NotificationReturnDto toReturnDto(Notification notification){
        NotificationReturnDto notificationReturnDto=new NotificationReturnDto();
        notificationReturnDto.setContent(notification.getContent());
        notificationReturnDto.setSubject(notification.getSubject());
        notificationReturnDto.setUser(notification.getUser());
        notificationReturnDto.setListOfModules(notification.getListOfModules());
        return notificationReturnDto;

    }
}
