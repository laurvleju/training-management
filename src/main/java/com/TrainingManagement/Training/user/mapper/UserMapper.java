package com.TrainingManagement.Training.user.mapper;

import com.TrainingManagement.Training.user.dto.userDto.UserCreateDto;
import com.TrainingManagement.Training.user.dto.userDto.UserReturnDto;
import com.TrainingManagement.Training.user.model.User;
import com.TrainingManagement.Training.user.model.UserType;

public class UserMapper {
    public static User toEntity(UserCreateDto userCreateDto){
        User user = new User();
        user.setFirstName(userCreateDto.getFirstName());
        user.setLastName(userCreateDto.getLastName());
        user.setUsername(userCreateDto.getUserName());
        user.setPassword(userCreateDto.getPassword());
        user.setType(UserType.ADMIN);
        return user;
    }

    public static UserReturnDto toReturnDto(User user){
        UserReturnDto userReturnDto=new UserReturnDto();
        userReturnDto.setUserName(user.getUsername());
        userReturnDto.setFirstName(user.getFirstName());
        userReturnDto.setLastName(user.getLastName());
        userReturnDto.setType(user.getType());
        userReturnDto.setCourse(user.getCourse());
        userReturnDto.setActive(user.getActive());
        userReturnDto.setId(user.getId());
        return userReturnDto;
    }
}
