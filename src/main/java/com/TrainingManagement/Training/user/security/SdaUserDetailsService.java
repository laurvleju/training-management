package com.TrainingManagement.Training.user.security;

import com.TrainingManagement.Training.user.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class SdaUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public SdaUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
      return new SdaUserDetails(userRepository.findByUsername(username));
    }
}
