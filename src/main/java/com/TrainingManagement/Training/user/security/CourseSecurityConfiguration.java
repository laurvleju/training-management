package com.TrainingManagement.Training.user.security;

import com.TrainingManagement.Training.user.model.UserType;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class CourseSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final SdaUserDetailsService sdaUserDetailsService;

    CourseSecurityConfiguration(SdaUserDetailsService sdaUserDetailsService) {
        this.sdaUserDetailsService = sdaUserDetailsService;
    }


    //unde e nevoie de authentificare
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("api/v1/course/post").hasRole(UserType.ADMIN.name())
//                .antMatchers("/api/v1/user").hasRole(UserType.ADMIN.name())
                .antMatchers("/api/v1/user/createuser").permitAll()
                .antMatchers("/api/v1/user/login").permitAll()
//                .antMatchers("/api/v1/user/").authenticated()
                .antMatchers("/api/v1/course/all").permitAll()
                .anyRequest().authenticated()
                .and().formLogin()
                .and().httpBasic();
    }

    //cine va face authentificarea
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(sdaUserDetailsService);
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

}
