package com.TrainingManagement.Training.user.controller;

import com.TrainingManagement.Training.user.service.NotificationService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/notification")
public class NotificationController {
    private final NotificationService notificationImpl;

    public NotificationController(NotificationService notificationImpl) {
        this.notificationImpl = notificationImpl;
    }
}
