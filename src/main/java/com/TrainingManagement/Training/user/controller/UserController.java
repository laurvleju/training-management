package com.TrainingManagement.Training.user.controller;

import com.TrainingManagement.Training.user.dto.userDto.UserCreateDto;
import com.TrainingManagement.Training.user.dto.userDto.UserReturnDto;
import com.TrainingManagement.Training.user.dto.userLoginDto.UserLoginDto;
import com.TrainingManagement.Training.user.model.User;
import com.TrainingManagement.Training.user.service.UserService;
import com.TrainingManagement.Training.user.service.serviceimplementation.UserImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userImpl;

    public UserController(UserImpl userImpl) {
        this.userImpl = userImpl;
    }

    @PostMapping("/createuser")
    public ResponseEntity create(@RequestBody UserCreateDto userCreateDto) {
        return ResponseEntity.ok(userImpl.createUser(userCreateDto));
    }

    @GetMapping("/")
    public ResponseEntity<UserReturnDto> getById(Integer id) {
        return ResponseEntity.ok(userImpl.findById(id));
    }

    @PostMapping("/login")
    public ResponseEntity<UserReturnDto> login(@RequestBody UserLoginDto userLoginDto){
        return ResponseEntity.ok(userImpl.login(userLoginDto));
    }

    @GetMapping("/getAll")
    public ResponseEntity<User> getAll(@RequestParam String username){
        return ResponseEntity.ok(userImpl.findByUsernameV2(username));
    }





}
