package com.TrainingManagement.Training.user.controller;

import com.TrainingManagement.Training.user.dto.applicationOfUserDto.ApplicationOfUserCreateDto;
import com.TrainingManagement.Training.user.dto.applicationOfUserDto.ApplicationOfUserReturnDto;
import com.TrainingManagement.Training.user.service.AplicationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/aplication")
public class ApplicationOfUserController {

    private final AplicationService aplicationService;

    public ApplicationOfUserController(AplicationService aplicationService) {
        this.aplicationService = aplicationService;
    }

    @PostMapping("/post")
    public ResponseEntity<ApplicationOfUserReturnDto> create(@RequestBody ApplicationOfUserCreateDto applicationOfUserCreateDto) {
        return ResponseEntity.ok(aplicationService.createApplication(applicationOfUserCreateDto));
    }

    @GetMapping("/getApplication")
    public ResponseEntity<ApplicationOfUserReturnDto> returnApplication(@RequestParam String username) {
        return ResponseEntity.ok(aplicationService.returnAplication(username));
    }
}


