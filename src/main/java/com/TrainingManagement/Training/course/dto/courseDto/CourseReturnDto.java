package com.TrainingManagement.Training.course.dto.courseDto;

import com.TrainingManagement.Training.course.model.Module;

import java.time.LocalDate;
import java.util.List;

public class CourseReturnDto {
    private Integer id;
    private String name;
    private List<Module> listOfModule;
    private String description;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Module> getListOfModule() {
        return listOfModule;
    }

    public void setListOfModule(List<Module> listOfModule) {
        this.listOfModule = listOfModule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
