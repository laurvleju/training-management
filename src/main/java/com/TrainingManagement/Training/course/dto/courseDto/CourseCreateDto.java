package com.TrainingManagement.Training.course.dto.courseDto;

import java.util.List;

public class CourseCreateDto {

    private String name;
    private int[] moduleId;
    private String description;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int[] getModuleId() {
        return moduleId;
    }

    public void setModuleId(int[] moduleId) {
        this.moduleId = moduleId;
    }
}


