package com.TrainingManagement.Training.course.dto.courseInstance;

import com.TrainingManagement.Training.user.model.User;

import java.util.List;

public class CourseInstanceDto {
    private Integer courseId;
    private String courseStartDate;
    private String courseEndDate;
    private String name;
    private List<User> userList;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseStartDate() {
        return courseStartDate;
    }

    public void setCourseStartDate(String courseStartDate) {
        this.courseStartDate = courseStartDate;
    }

    public String getCourseEndDate() {
        return courseEndDate;
    }

    public void setCourseEndDate(String courseEndDate) {
        this.courseEndDate = courseEndDate;
    }
}
