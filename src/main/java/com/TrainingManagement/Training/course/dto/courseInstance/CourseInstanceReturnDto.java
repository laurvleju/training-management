package com.TrainingManagement.Training.course.dto.courseInstance;

import com.TrainingManagement.Training.course.model.ModuleInstance;
import com.TrainingManagement.Training.user.model.User;

import java.util.List;

public class CourseInstanceReturnDto {
    private String courseName;
    private String courseDescription;
    private List<ModuleInstance> list;
    private String startDate;
    private String endDate;
    private List<User> userList;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public List<ModuleInstance> getList() {
        return list;
    }

    public void setList(List<ModuleInstance> list) {
        this.list = list;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
