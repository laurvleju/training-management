package com.TrainingManagement.Training.course.mapper.instance;

import com.TrainingManagement.Training.course.model.Course;
import com.TrainingManagement.Training.course.model.Module;
import com.TrainingManagement.Training.course.model.ModuleInstance;
import com.TrainingManagement.Training.course.repository.ModuleInstanceRepository;

import java.util.ArrayList;
import java.util.List;

public class ModuleInstanceMapper {

    //    public static List<ModuleInstance> toModule(Course course, Date startDate, Date endDate) {
    public static List<ModuleInstance> toModule(Course course) {
        List<ModuleInstance> listModulesInstance = new ArrayList<>();
        for(Module module: course.getListOfModules()){
            ModuleInstance moduleInstance = new ModuleInstance();
            moduleInstance.setModule(module);
            listModulesInstance.add(moduleInstance);
        }
        return listModulesInstance;
    }
}
