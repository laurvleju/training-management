package com.TrainingManagement.Training.course.mapper;

import com.TrainingManagement.Training.course.dto.courseDto.CourseCreateDto;
import com.TrainingManagement.Training.course.dto.courseDto.CourseReturnDto;
import com.TrainingManagement.Training.course.model.Course;

public class CourseMapper {

    public static Course toEntity(CourseCreateDto courseCreateDto) {
        Course course = new Course();

        course.setCourseName(courseCreateDto.getName());
        course.setDescription(courseCreateDto.getDescription());

        return course;
    }

    public static CourseReturnDto toReturn(Course course) {
        CourseReturnDto courseReturnDto = new CourseReturnDto();

        courseReturnDto.setId(course.getId());
        courseReturnDto.setName(course.getCourseName());
        courseReturnDto.setDescription(course.getDescription());
        courseReturnDto.setListOfModule(course.getListOfModules());

        return courseReturnDto;
    }
}
