package com.TrainingManagement.Training.course.mapper;

import com.TrainingManagement.Training.course.dto.moduleDto.ModuleCreateDto;
import com.TrainingManagement.Training.course.dto.moduleDto.ModuleReturnDto;
import com.TrainingManagement.Training.course.model.Module;

public class ModuleMapper {
    public static Module toEntity(ModuleCreateDto moduleCreateDto){
        Module module = new Module();

        module.setName(moduleCreateDto.getName());
        module.setDescription(moduleCreateDto.getDescription());
        module.setDescription(moduleCreateDto.getDescription());

        return module;
    }

    public static ModuleReturnDto toReturnDto(Module module){
        ModuleReturnDto moduleReturnDto = new ModuleReturnDto();

        moduleReturnDto.setId(module.getId());
        moduleReturnDto.setName(module.getName());
        moduleReturnDto.setDescription(module.getDescription());

        return moduleReturnDto;
    }
}
