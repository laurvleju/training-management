package com.TrainingManagement.Training.course.mapper.instance;

import com.TrainingManagement.Training.course.dto.courseInstance.CourseInstanceDto;
import com.TrainingManagement.Training.course.dto.courseInstance.CourseInstanceReturnDto;
import com.TrainingManagement.Training.course.exception.CourseNotFoundException;
import com.TrainingManagement.Training.course.model.Course;
import com.TrainingManagement.Training.course.model.CourseInstance;
import com.TrainingManagement.Training.course.model.ModuleInstance;

import java.util.Date;
import java.util.List;

public class CourseInstanceMapper {

    public static CourseInstance toEntity(Course course, CourseInstanceDto courseInstanceDto){
        CourseInstance courseInstance = new CourseInstance();
        List<ModuleInstance> list = ModuleInstanceMapper.toModule(course);
        courseInstance.setCourse(course);
        courseInstance.setModules(list);
        courseInstance.setStartDate(courseInstanceDto.getCourseStartDate());
        courseInstance.setEndDate(courseInstanceDto.getCourseEndDate());
        courseInstance.setName(courseInstanceDto.getName());
        return courseInstance;
    }

    public static CourseInstanceReturnDto toReturn(CourseInstance courseInstance){
        CourseInstanceReturnDto courseInstanceReturnDto = new CourseInstanceReturnDto();

        courseInstanceReturnDto.setCourseName(courseInstance.getCourse().getCourseName());
        courseInstanceReturnDto.setCourseDescription(courseInstance.getCourse().getDescription());
        courseInstanceReturnDto.setList(courseInstance.getModules());
        courseInstanceReturnDto.setStartDate(courseInstance.getStartDate());
        courseInstanceReturnDto.setEndDate(courseInstance.getEndDate());
        courseInstanceReturnDto.setCourseName(courseInstance.getName());
        courseInstanceReturnDto.setUserList(courseInstance.getUsers());
        return courseInstanceReturnDto;
    }
}
