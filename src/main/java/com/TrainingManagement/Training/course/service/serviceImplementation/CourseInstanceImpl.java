package com.TrainingManagement.Training.course.service.serviceImplementation;

import com.TrainingManagement.Training.course.dto.courseInstance.CourseInstanceDto;
import com.TrainingManagement.Training.course.dto.courseInstance.CourseInstanceReturnDto;
import com.TrainingManagement.Training.course.exception.CourseNotFoundException;
import com.TrainingManagement.Training.course.mapper.instance.CourseInstanceMapper;
import com.TrainingManagement.Training.course.mapper.instance.ModuleInstanceMapper;
import com.TrainingManagement.Training.course.model.Course;
import com.TrainingManagement.Training.course.model.CourseInstance;
import com.TrainingManagement.Training.course.model.ModuleInstance;
import com.TrainingManagement.Training.course.repository.CourseInstanceRepository;
import com.TrainingManagement.Training.course.repository.CourseRepository;
import com.TrainingManagement.Training.course.repository.ModuleInstanceRepository;
import com.TrainingManagement.Training.course.service.CourseInstanceService;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class CourseInstanceImpl implements CourseInstanceService {

    private final CourseRepository courseRepository;
    private final CourseInstanceRepository courseInstanceRepository;
    private final ModuleInstanceRepository moduleInstanceRepository;
    private ModuleInstanceImpl moduleInstance;
    private List<CourseInstance> listOfAllCourse = new ArrayList<>(15);

    public CourseInstanceImpl(CourseRepository courseRepository, CourseInstanceRepository courseInstanceRepository, ModuleInstanceRepository moduleInstanceRepository, ModuleInstanceImpl moduleInstance) {
        this.courseRepository = courseRepository;
        this.courseInstanceRepository = courseInstanceRepository;
        this.moduleInstanceRepository = moduleInstanceRepository;
        this.moduleInstance = moduleInstance;
    }

    @Override
    public CourseInstanceReturnDto createInstanceCourse(CourseInstanceDto courseInstanceDto) {
        Course course = courseRepository.findById(courseInstanceDto.getCourseId()).orElseThrow(CourseNotFoundException::new);
        CourseInstance courseInstance = CourseInstanceMapper.toEntity(course, courseInstanceDto);
        listOfAllCourse.add(courseInstance);
        courseInstanceRepository.save(courseInstance);
        return CourseInstanceMapper.toReturn(courseInstance);
    }

    @Override
    public List<CourseInstanceReturnDto> getAll() {
        Iterable<CourseInstance> all = courseInstanceRepository.findAll();
        List<CourseInstanceReturnDto> list = new ArrayList<>();
        for(CourseInstance cr: all){
            list.add(CourseInstanceMapper.toReturn(cr));
        }
        return list;
    }


}
