package com.TrainingManagement.Training.course.service.serviceImplementation;

import com.TrainingManagement.Training.course.dto.courseDto.CourseCreateDto;
import com.TrainingManagement.Training.course.dto.courseDto.CourseReturnDto;
import com.TrainingManagement.Training.course.exception.CourseNotFoundException;
import com.TrainingManagement.Training.course.exception.ModuleNotFoundException;
import com.TrainingManagement.Training.course.mapper.CourseMapper;
import com.TrainingManagement.Training.course.model.Course;
import com.TrainingManagement.Training.course.model.Module;
import com.TrainingManagement.Training.course.repository.CourseRepository;
import com.TrainingManagement.Training.course.repository.ModuleRepository;
import com.TrainingManagement.Training.course.service.CourseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CourseImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final ModuleRepository moduleRepository;

    public CourseImpl(CourseRepository courseRepository, ModuleRepository moduleRepository) {
        this.courseRepository = courseRepository;
        this.moduleRepository = moduleRepository;
    }

    @Override
    public CourseReturnDto create(CourseCreateDto courseCreateDto) {
        List<Module> listOfModule = new ArrayList<>();

        for (Integer id : courseCreateDto.getModuleId()) {
            listOfModule.add(moduleRepository.findById(id).orElseThrow(ModuleNotFoundException::new));
        }
        Course course = CourseMapper.toEntity(courseCreateDto);
        course.setListOfModules(listOfModule);
        courseRepository.save(course);

        return CourseMapper.toReturn(course);
    }

    @Override
    public List<Course> getAll() {
        Iterable<Course> all = courseRepository.findAll();
        return (List<Course>) all;
    }

    @Override
    public CourseReturnDto getById(Integer courseId) {
        Course course = courseRepository.findById(courseId).orElseThrow(CourseNotFoundException::new);
        return CourseMapper.toReturn(course);
    }

    @Override
    public List<CourseReturnDto> getCourses() {
        List<Course> courses = (List<Course>) courseRepository.findAll();
        List<CourseReturnDto> list = new ArrayList<>();
        for (Course cours : courses) {
            CourseReturnDto ret = CourseMapper.toReturn(cours);
            list.add(ret);
        }
        return list;
    }


}
