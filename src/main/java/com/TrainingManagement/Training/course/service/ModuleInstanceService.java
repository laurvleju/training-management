package com.TrainingManagement.Training.course.service;

import com.TrainingManagement.Training.course.dto.moduleDto.ModuleReturnDto;
import com.TrainingManagement.Training.course.model.Course;
import com.TrainingManagement.Training.course.model.Module;

import java.util.List;

public interface ModuleInstanceService {
    public String showModules(Module module);

}
