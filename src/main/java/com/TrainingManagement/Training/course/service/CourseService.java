package com.TrainingManagement.Training.course.service;

import com.TrainingManagement.Training.course.dto.courseDto.CourseCreateDto;
import com.TrainingManagement.Training.course.dto.courseDto.CourseReturnDto;
import com.TrainingManagement.Training.course.model.Course;

import java.util.List;

public interface CourseService {

    CourseReturnDto create(CourseCreateDto courseCreateDto);
    List<Course> getAll();
    CourseReturnDto getById(Integer courseId);
    List<CourseReturnDto> getCourses();
}
