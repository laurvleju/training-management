package com.TrainingManagement.Training.course.service.serviceImplementation;

import com.TrainingManagement.Training.course.dto.moduleDto.ModuleCreateDto;
import com.TrainingManagement.Training.course.dto.moduleDto.ModuleReturnDto;
import com.TrainingManagement.Training.course.exception.CourseNotFoundException;
import com.TrainingManagement.Training.course.exception.ModuleNotFoundException;
import com.TrainingManagement.Training.course.mapper.ModuleMapper;
import com.TrainingManagement.Training.course.model.Course;
import com.TrainingManagement.Training.course.model.Module;
import com.TrainingManagement.Training.course.repository.CourseRepository;
import com.TrainingManagement.Training.course.repository.ModuleRepository;
import com.TrainingManagement.Training.course.service.ModuleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class ModuleImpl implements ModuleService {

    private final ModuleRepository moduleRepository;
    private CourseRepository courseRepository;

    public ModuleImpl(ModuleRepository moduleRepository, CourseRepository courseRepository) {
        this.moduleRepository = moduleRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public ModuleReturnDto createModule(ModuleCreateDto moduleCreateDto) {
        return ModuleMapper.toReturnDto(moduleRepository.save(ModuleMapper.toEntity(moduleCreateDto)));
    }

    @Override
    public ModuleReturnDto findById(Integer id) {
        return ModuleMapper.toReturnDto(moduleRepository.findById(id).orElseThrow(() -> new ModuleNotFoundException()));
    }

    @Override
    public List<Module> getAll() {
        Iterable<Module> all = moduleRepository.findAll();
        return (List<Module>) all;
    }

}
