package com.TrainingManagement.Training.course.service.serviceImplementation;

import com.TrainingManagement.Training.course.mapper.instance.ModuleInstanceMapper;
import com.TrainingManagement.Training.course.model.Course;
import com.TrainingManagement.Training.course.model.Module;
import com.TrainingManagement.Training.course.model.ModuleInstance;
import com.TrainingManagement.Training.course.repository.ModuleInstanceRepository;
import com.TrainingManagement.Training.course.service.ModuleInstanceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModuleInstanceImpl implements ModuleInstanceService {

    private final ModuleInstanceRepository moduleInstanceRepository;

    public ModuleInstanceImpl(ModuleInstanceRepository moduleInstanceRepository) {
        this.moduleInstanceRepository = moduleInstanceRepository;
    }

    @Override
    public String showModules(Module module) {
        return null;
    }

//    public List<ModuleInstance> returnList(Course course){
//        moduleInstanceRepository.saveAll(ModuleInstanceMapper.toModule(course));
//        return ModuleInstanceMapper.toModule(course);
//    }

}
