package com.TrainingManagement.Training.course.service;

import com.TrainingManagement.Training.course.dto.moduleDto.ModuleCreateDto;
import com.TrainingManagement.Training.course.dto.moduleDto.ModuleReturnDto;
import com.TrainingManagement.Training.course.model.Module;

import java.util.List;

public interface ModuleService {
    ModuleReturnDto createModule(ModuleCreateDto moduleCreateDto);
    ModuleReturnDto findById(Integer id);

    List<Module> getAll();

}
