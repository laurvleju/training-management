package com.TrainingManagement.Training.course.service;

import com.TrainingManagement.Training.course.dto.courseInstance.CourseInstanceDto;
import com.TrainingManagement.Training.course.dto.courseInstance.CourseInstanceReturnDto;
import com.TrainingManagement.Training.course.model.CourseInstance;
import com.TrainingManagement.Training.course.model.Module;

import java.util.Date;
import java.util.List;

public interface CourseInstanceService {

    CourseInstanceReturnDto createInstanceCourse(CourseInstanceDto courseInstanceDto);
    List<CourseInstanceReturnDto> getAll();

}
