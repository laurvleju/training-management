package com.TrainingManagement.Training.course.repository;

import com.TrainingManagement.Training.course.model.ModuleInstance;
import org.springframework.data.repository.CrudRepository;

public interface ModuleInstanceRepository extends CrudRepository<ModuleInstance,Integer> {
}
