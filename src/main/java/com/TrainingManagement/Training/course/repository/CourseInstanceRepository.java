package com.TrainingManagement.Training.course.repository;

import com.TrainingManagement.Training.course.model.CourseInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseInstanceRepository extends CrudRepository<CourseInstance,Integer> {
}
