package com.TrainingManagement.Training.course.repository;

import com.TrainingManagement.Training.course.model.Module;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModuleRepository extends CrudRepository<Module,Integer> {
}
