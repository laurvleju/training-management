package com.TrainingManagement.Training.course.exception;

public class ModuleNotFoundException extends RuntimeException{
    public ModuleNotFoundException() {
        super("Module not found.");
    }
}
