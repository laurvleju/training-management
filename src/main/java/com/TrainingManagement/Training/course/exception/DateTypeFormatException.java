package com.TrainingManagement.Training.course.exception;

public class DateTypeFormatException extends RuntimeException{
    public DateTypeFormatException() {
        super("Date format not supported");
    }
}
