package com.TrainingManagement.Training.course.controller;

import com.TrainingManagement.Training.course.dto.courseInstance.CourseInstanceDto;
import com.TrainingManagement.Training.course.dto.courseInstance.CourseInstanceReturnDto;
import com.TrainingManagement.Training.course.model.CourseInstance;
import com.TrainingManagement.Training.course.service.serviceImplementation.CourseInstanceImpl;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("api/v1/courseInstance")
public class CourseInstanceController {

    private final CourseInstanceImpl courseInstance;

    public CourseInstanceController(CourseInstanceImpl courseInstance) {
        this.courseInstance = courseInstance;
    }

    @PostMapping("/post")
    public ResponseEntity<CourseInstanceReturnDto> createInstance(@RequestBody CourseInstanceDto courseInstanceDto) {
        return ResponseEntity.ok(courseInstance.createInstanceCourse(courseInstanceDto));
    }

    @GetMapping("/all")
    public ResponseEntity<List<CourseInstanceReturnDto>> getAll(){
        return ResponseEntity.ok(courseInstance.getAll());
    }
}
