package com.TrainingManagement.Training.course.controller;

import com.TrainingManagement.Training.course.dto.moduleDto.ModuleCreateDto;
import com.TrainingManagement.Training.course.dto.moduleDto.ModuleReturnDto;
import com.TrainingManagement.Training.course.model.Module;
import com.TrainingManagement.Training.course.service.ModuleService;
import com.TrainingManagement.Training.course.service.serviceImplementation.ModuleImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/module")
public class ModuleController {

    private final ModuleService moduleImpl;

    public ModuleController(ModuleImpl moduleImpl) {
        this.moduleImpl = moduleImpl;
    }

    @PostMapping("/post")
    public ResponseEntity create(@RequestBody ModuleCreateDto moduleCreateDto) {
        return ResponseEntity.ok(moduleImpl.createModule(moduleCreateDto));
    }

    @GetMapping("/id")
    public ResponseEntity findById(@RequestParam Integer id) {
        return ResponseEntity.ok(moduleImpl.findById(id));
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Module>> getAll() {
        return ResponseEntity.ok(moduleImpl.getAll());
    }
}
