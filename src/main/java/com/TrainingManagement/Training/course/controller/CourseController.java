package com.TrainingManagement.Training.course.controller;

import com.TrainingManagement.Training.course.dto.courseDto.CourseCreateDto;
import com.TrainingManagement.Training.course.dto.courseDto.CourseReturnDto;
import com.TrainingManagement.Training.course.service.CourseService;
import com.TrainingManagement.Training.course.service.serviceImplementation.CourseImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/course")
public class CourseController {

    private final CourseService courseImpl;

    public CourseController(CourseImpl courseImpl) {
        this.courseImpl = courseImpl;
    }

    @PostMapping("/post")
    public ResponseEntity create(@RequestBody CourseCreateDto courseCreateDto) {
        return ResponseEntity.ok(courseImpl.create(courseCreateDto));
    }

    @GetMapping("/")
    public ResponseEntity<CourseReturnDto> getCourseById(@RequestParam Integer courseId) {
        return ResponseEntity.ok(courseImpl.getById(courseId));
    }

    @GetMapping("/all")
    public ResponseEntity<List<CourseReturnDto>> getCourses(){
        return ResponseEntity.ok(courseImpl.getCourses());
    }
}
