package com.TrainingManagement.Training.course.model;

import javax.persistence.*;

@Entity
public class ModuleInstance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne()
    private Module module;
    @ManyToOne()
    private CourseInstance courses;


    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

}
