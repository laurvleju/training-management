package com.TrainingManagement.Training.course.model;

import com.TrainingManagement.Training.user.model.User;

import javax.persistence.*;
import java.util.List;


@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "course_name")
    private String courseName;
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "course_id")
    private List<Module> listOfModules;
//    @OneToMany(mappedBy = "course")
//    private List<User> listOfUsers;
    @Column(name = "description")
    private String description;
//    @OneToMany(mappedBy = "course")
//    private List<CourseInstance> courseInstance;

    public Integer getId() {
        return id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public List<Module> getListOfModules() {
        return listOfModules;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setListOfModules(List<Module> listOfModules) {
        this.listOfModules = listOfModules;
    }

}
