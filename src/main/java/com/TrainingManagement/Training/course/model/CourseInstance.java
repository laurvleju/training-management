package com.TrainingManagement.Training.course.model;

import com.TrainingManagement.Training.user.model.ApplicationOfUser;
import com.TrainingManagement.Training.user.model.User;

import javax.persistence.*;
import java.util.List;

@Entity
public class CourseInstance {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "course_name")
    private String name;
    @ManyToOne()
    private Course course;
    @OneToMany(cascade = CascadeType.ALL)
    private List<ModuleInstance> modules;
    @Column(name = "start_date")
    private String startDate;
    @Column(name = "end_date")
    private String endDate;
    @OneToMany(mappedBy = "course")
    private List<User> users;
    @OneToOne(mappedBy = "course")
    private ApplicationOfUser applicationOfUser;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public List<ModuleInstance> getModules() {
        return modules;
    }

    public void setModules(List<ModuleInstance> modules) {
        this.modules = modules;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public ApplicationOfUser getApplicationOfUser() {
        return applicationOfUser;
    }

    public void setApplicationOfUser(ApplicationOfUser applicationOfUser) {
        this.applicationOfUser = applicationOfUser;
    }
}
